#!/usr/bin/python3

import os
import sys
import subprocess

gitlab_user=os.getenv("GITLAB_USER_LOGIN")
gitlab_email=os.getenv("GITLAB_USER_EMAIL")
users = []
emails = []

with open(sys.argv[1]) as f:
    for line in f:
        items=line.split()
        emails.append(items[0])
        users.append(items[1])

if not gitlab_user in users:
    print("user %s has not verified the CLA" % gitlab_user)
    sys.exit(1)

if not gitlab_email in emails:
    print("email %s has not verified the CLA" % gitlab_email)
    sys.exit(1)

print("User %s %s has signed CLA✔️" % (gitlab_user, gitlab_email))

for git_email in subprocess.check_output("git log --pretty=format:%aE|sort|uniq",
        encoding="utf-8", shell=True).split('\n'):
    if git_email=="":
        continue
    if not git_email in emails:
        print("%s from git history has not verified the CLA" % git_email)
        sys.exit(1)

print("Git log verified for CLA✔")

